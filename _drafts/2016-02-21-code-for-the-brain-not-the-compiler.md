---
layout: post
title: Code For The Brain, Not The Compiler
---

The following is going to be highly opinionated and subjective. My own personal feelings, but ones I believe deeply enough in to try and impart on others.

When I write code I am meticulous with my indentation, intentional with my whitespace and frugal with the number of lines between any pair of `{}`. When I name something, before I've even left the line I'll have thought of 3 alternatives. By the end of the block I'll have tried 2 of them.

I hesitate to comment my code — I see it as failure — failure to adequately and clearly express my intention to others using the grammar of the programming language alone. Having to resort to the far more ambiguous spoken languages.

I'd much rather restructure code to be clearer, commenting is a last resort for genuinely complex business logic.

Being a human I obviously make mistakes and have off days. By and large though this has become second nature.

I don't do these things because they are quick or convenient. It's much easier to write code that _just_ compiles than it is code that can be _read_. But, if all code was written with the mindfulness and foreground thought that it was for **humans**, the net gain would far outweigh the up–front cost.

### In practice
I'll try to make this point by showing you some code and refactoring it with you.

{% gist JamieAP/5eae18149afd2f1c1fa6 %}

Now, the intention of this code is pretty simple. It's defining a method that can turn some identifiers into a mapping of their associated records pulled out of a database.

How it achieves that is anything but clear. I am of the opinion one should be able to glance at a fragment of code and determine, almost instantaneously, if it is pertinent to their interest. Only perhaps needing to spend a little more time understanding the nuances of if they decide yes, it is.

What can we do to make that a possibility here?

Firstly, that multi-line lambda sticks out like a sore thumb. It trails far off to the right, indented to 10 levels or 40 spaces. It makes it hard to interpret the flow of the method based off of the indentation. I'll come back to why thats important.

Let's go ahead and pull it out.

<script src="https://gist.github.com/JamieAP/d0ca1fbafbb6248163a4.js"></script>

I've extracted the lambda we were inlining to `Stream.map()` into its own function.

This has made the first method less scary looking but barely any more comprehensible. I've let other parts of this code use the very general and probably (re)usable elsewhere `resolveColumnListForId()` function.

### On Streams

When using the functional style `Stream` in Java 8 you are naturally writing code that has less side effects, is tighter in scope and clearer in intention.

A `Stream` has some basic functional operations that are understood by the vast majority of developers. Everyone knows `map()` transforms elements into different ones. Everyone knows `filter()` drops elements that don't meet a criteria. Everyone knows what to expect of `limit()`, `count()`, `skip()`, and `sort()`.

The [cognitive load](https://www.wikiwand.com/en/Cognitive_load) of anyone reading code that uses `Stream` is massively reduced. They can instantly understand the process your code is implementing. Even without knowing exactly _how_ it does `sort()` or `filter()` and so on.

However, this clean illustration of your intention is obscured when you put multi-line lambdas inline where they ought to be their own function. Don't forget [method references](https://jamiep.net/2015/03/30/java-8-method-references/) are a thing.

Back to our example, the snippet below is the same as the last, before you try to diff.

<script src="https://gist.github.com/JamieAP/d0ca1fbafbb6248163a4.js"></script>

### Logical Steps
I've also extracted the inlined database query into its own variable. This call is a discrete and important part of the functionality this method provides. It's a logical step that deserves to be understood on its own. By inlining it, we are making it harder to understand the flow.

This method now reads as: get a `ListenableFuture` for some data from the database and then asynchronously transform that result.

It's much easier to mentally parse that flow when the code guides you through it, rather than mangling it all together. Variables can be used to highlight these logical steps to a reader.

Moving on to that first method.

<script src="https://gist.github.com/JamieAP/d9d1fd11598d5252e27f.js"></script>

It's become two separate methods. My biggest issue with this first method was the almost total lack of horizontal whitespace.

### Vertical vs Horizontal Whitespace

I believe you should be conservative with your horizontal whitespace and liberal with your vertical.

If you think of writing code in the way you would write a book or an article this makes sense. You try to avoid cramming
too much text into an unbroken sentence (a line of code). If you think of `.` as a comma, its easier to see where you're doing too much
in a single line.

You might say "but if I don't chain calls on the same line, my block becomes extremely long". Well that's a good thing, you've probably uncovered a block of code that is trying to do too much. When you chain tons of successive method calls way over the ruler, you're hiding the fact that really this block should be two separate blocks.

It's also far easier to read code that descends down the screen, not trails off to the right.

For example, which is easier to understand?

<script src="https://gist.github.com/JamieAP/89d615946d847d67216b.js"></script>

or

<script src="https://gist.github.com/JamieAP/5852d007e5b9efffcb61.js"></script>

Some might argue this kind of indentation is excessive. I personally find it much
easier to read when I can see the nested method calls expressed as a level of indentation.

Some might agree with me in principle but believe the `)`s can be pushed up the last line, like so:

<script src="https://gist.github.com/JamieAP/c0fc298633430dde824e.js"></script>

I have less of an argument to make in favour of the `)` being on their own line.
You can still see the indentation and thus nested method calls. A chain of `)` just looks ugly to me,
it's a personal preference.

Back to the refactored code:

<script src="https://gist.github.com/JamieAP/d9d1fd11598d5252e27f.js"></script>

### Miscellany
I've also dropped the `throws` on the first method that was unnecessary...

I've replaced `StreamSupport.stream(longIds.spliterator(), false)` with `ImmutableList.copyOf(longIds).stream()`.
I think you'll agree that reads nicer and consumes less horizontal whitespace.
In a code base that uses `ImmutableCollection` extensively this is likely to be a no-op.

The `Stream` calls have been chained and indented to make use of vertical whitespace.

The final result of refactoring is this:

<script src="https://gist.github.com/JamieAP/7361070ce8ff762c43df.js"></script>

Yes, we've turned 19 lines into 46 lines. But that's not a bad thing. This code now occupies a number of lines representative of the work it is doing.

Which would you rather open up and have to make changes to? Which will be faster for you to comprehend?

If you aren't compacting your code into as few lines for human beings, who are you doing it for?
The compiler certainly isn't going to thank you. The bytecode isn't going to run measurably faster if it does at all.

### Empathy

What I'm preaching here is empathy when writing code. It may very well be that the un–refactored code can be easily understood by those senior enough to have endured far worse.

But what about juniors or people unfamiliar to the language? It is unnecessarily making it harder for them to get what they want done. Its also making it harder for them to learn good habits.

That to me makes this objectively bad code.

### In Conclusion

If nothing else consistency should be your aim. Consistent code, even if it's not beautiful code, is easier to follow than a mix and match.

Im not a fan of style guides militantly auto-checked against every commit. I feel they will fail in an unacceptable number of cases to produce clean code. But they do enforce consistency and that can ingrain good habits.

Thats about all I have to say for how I like to write code for now. If you got this far, thanks for listening to me.
