---
layout: post
title: Java 8 Method References
---

Originally posted on [MetaBroadcast's blog](https://metabroadcast.com/blog/java-8-method-references)

The following is a fairly reasonable block of code that one might find in a project using Guava.

{% gist mbst-jamie/8e4eedebdf77995a1af3 %}

It's fairly hard to tell just from glancing at this code what it is actually doing. First it's transforming a group of Objects into different Objects, then it's filtering them based on the result of some predicate and then taking the first 5 results. Finally it forces the Iterables.doSomething()s to be evaluated by copying the result into an ImmutableList.

The main problem for this code's readability is those pesky anonymous classes for the Predicate <object>and Function<object, object="">. The problem could be ameliorated by extracting them into constants or, if that's not possible static factory methods. However that is just moving the problem elsewhere. There is one line of code in each of those anonymous classes that we actually care about.

Lambdas and Stream can help things...

{% gist mbst-jamie/55b3c025d63ede096abb %}

So that's turned 10 lines of code into 5\. However `s -> changeThing(s)` this still feels like it could be less verbose, doesn't it? Stream.map() and filter() expects a Function <t, u="">and a Predicate <t>to call respectively, which are both [@FunctionalInterfaces](https://docs.oracle.com/javase/8/docs/api/java/lang/FunctionalInterface.html) and expose a single method. In which case, why are we telling Java _how_ to use something if there is only one way to use it?</t></t,>

### Method References

{% gist mbst-jamie/00dba9a956d9d1331eca %}

Rather than telling the Stream how to use the method, we simply say there exists a method in this class called changeThing and testThing, go use them.

Some more examples...

{% gist mbst-jamie/d52d0fc036bde16514a4 %}

This is useful because it means we don't need to create an anonymous class just to invoke an existing method, we can simply reference it to use it as a lambda expression. In cases where you are using an existing project with Guava that has been upgraded to use Java 8 and you don't want to mix Iterables and Stream this lets you take advantage of method references to make the code a little less verbose.
