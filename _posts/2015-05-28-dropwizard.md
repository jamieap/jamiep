---
layout: post
title: Dropwizard
---
Originally posted on [MetaBroadcast's blog](https://metabroadcast.com/blog/dropwizard).

As you may know, we have many REST APIs running on the JVM. The majority of which use the <a href="http://projects.spring.io/spring-framework/">Spring MVC framework</a>. We've very much a love/hate relationships with Spring (mostly hate).

An entire series of posts could be written about things we don't like about Spring, but to save time and sanity, I'll summarise as: too much boilerplate and configuration around merely getting the framework to serve HTTP requests. XML config files and <code>ManagerServiceFactoryBeanImplFactory</code> take time to understand and set up, leaving you less time to solve the problem that your application owes its existence to.  We use annotations in most places to configure our Spring apps, but it's still a pain to have to deal with in general.

We've now moved two of our projects onto [Dropwizard](http://www.dropwizard.io) and are so far extremely happy with what we've seen.

### What is Dropwizard?

It's a combination of things really as the Getting Started page will tell you. It provides [Jetty](http://www.eclipse.org/jetty/) for HTTP, [Jackson](https://github.com/FasterXML/jackson) for JSON, [Jersey](https://jersey.java.net/) for REST and so on. The power and convenience of Dropwizard comes from it's hiding and pre-configuration of the above things right out of the box. To go from nothing to something serving API requests in Dropwizard doesn't require you to write a line of code setting up a Jetty or configuring Jackson.

Everything you need is exposed cleanly, everything you don't is hidden away. This leaves you to start writing code to solve problems almost immediately. The two projects I mentioned above were already using Jetty and Jersey, and so their migrations were fairly trivial. We will be moving [Atlas Deer](https://github.com/atlasapi/atlas-deer) from Spring to Dropwizard sometime in the near future. I'll report back on how that goes and with any useful tips for moving from one to the other.

### Example!

{% gist mbst-jamie/b61c71774246ab1cf836 %}

The above code, plus a very simple [Configuration class and YAML](http://www.dropwizard.io/manual/core.html#configuration), is all you need to start serving requests from an endpoint in Dropwizard. On top of that there is a [long list](http://www.dropwizard.io/manual/index.html) of built-in, out of the box features that most if not all applications of this type would like.

An admin servlet is automatically configured and started, from which tasks and health checks can be easily built on top of. Other features include but are not limited to:

* Logging
* Object lifecycle management
* Configuration validation
* JVM and resource metrics
* ASCII art printing at start up

All come for free and only require you to implement the bit you care about.

I'll save going into much more depth for after we've moved a Spring application onto Dropwizard, as I expect there will be far more to show. But so far, we would seriously recommend checking it out!
