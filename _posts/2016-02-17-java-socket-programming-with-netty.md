---
layout: post
title: Java Socket Programming With Netty
---
Originally posted on [MetaBroadcast's blog](https://metabroadcast.com/blog/java-socket-programming-with-netty).

It’s (hopefully) quite infrequent that one needs to work with network sockets directly to chuck bytes around. Normally in an application you’ll use an existing application–level protocol like REST over HTTP to pass data around. The reasons for this include, but are not limited to; convenience, reliability, interoperability and sanity.

That said, should you find yourself in a position where you need better performance or more flexibility than an existing protocol, it’s useful to know where to start.

For example, I used it recently in an [IoT](https://www.wikiwand.com/en/Internet_of_Things) project where it would have been time consuming and inefficient to deal with HTTP clients in embedded C++ code.

### Netty

[Netty](http://netty.io/) is an NIO (non–blocking input/output) client–server framework for Java. It simplifies the process of writing servers and clients that talk to each other under the hood using your typical `DatagramSocket`, `ServerSocket` and `Socket` classes. In this example I’ll show you how to write a very simple server that will accept connections over a TCP port, read and decode JSON and do something with it.

In real life you’re probably more likely to use something binary like [Thrift](https://thrift.apache.org/), [Protocol Buffers](https://github.com/google/protobuf) or [Smile](http://wiki.fasterxml.com/SmileFormat), instead of JSON.

### <a id="getting_started_23"></a>Getting started

I am assuming you have imported Netty using the dependency manager of your choice and are ready to start typing code.

{% gist JamieAP/fc88328a57003a819350 %}

First off we need instances of `NioEventLoopGroup`. This class implements a multi-threaded [Event Loop](https://www.wikiwand.com/en/Event_loop), that is, something that constantly and frequently polls IO abstractions for stuff to do like read data or start a new connection. There is also the `EpollEventLoopGroup` available if you're on Linux, which makes use of the more performant [Epoll](https://www.wikiwand.com/en/Epoll).

We need two of them, one to accept new connections and one to handle existing connections. If you’ve worked with an HTTP server you’ll know it typically uses the same thing.

### <a id="configuring_the_server_42"></a>Configuring the server

Next we must configure the server proper. Lets walk through.

{% gist JamieAP/ba4bcc0862e1dbac5369 %}
`ServerBootstrap` is a helper of sorts that lets you avoid configuring every single aspect of the highly complex `ServerChannel` implementations. Basically does what it says on the tin, it bootstraps a server for us.

It needs setting up with a few things, first we give it the event loops we created earlier which allows our server to accept and handle connections.

Next is a call to `.channel()` with a class. Netty will creates instances of this class and uses them to accept new connections. In this case that’s `NioServerSocketChannel` which is an implementation of `ServerChannel`.

Then a call to `.childHandler()` with an instance of `ChannelHandler`. This is where interesting things will happen, it sets up the pipeline that accepted connections are handled through. Here I’m using a class called `MySocketInitialiser`, my own creation, we’ll come back to this.

Calls to `.option()` let us set server–related TCP options. In this case `SO_BACKLOG` tells the server to refuse connections if it already has 5 queued up.

Finally calls to `.childOption()` let us set client–related TCP options. `SO_KEEPALIVE` tells clients to keep their connections open with [keepalive](https://www.wikiwand.com/en/Keepalive) packets.

We then start the server by telling it to bind to a port at the local address and call `.sync()` to wait for the server to shutdown.

### <a id="setting_up_a_pipeline_87"></a>Setting up a pipeline

Back to `MySocketInitialiser` to see where the magic happens.
{% gist JamieAP/175d9cfc54dab8532d56 %}
The `initChannel()` method of this class is called by Netty whenever it receives a new connection. A `SocketChannel` is simply the [channel](http://netty.io/4.0/api/io/netty/channel/Channel.html) abstraction over a TCP/IP socket.

Each `SocketChannel` has a [pipeline](http://netty.io/4.0/api/io/netty/channel/ChannelPipeline.html) associated with it. You can think of think of the pipeline as an ordered list of handlers with each feeding its output as the input to the next one. There are caveats to this but we can ignore them for now.

In the example pipeline above we have in order the following:

1.  A `LineBasedFrameDecoder`, this delimits messages by detecting newlines bytes (i.e. \n or \r\n)
2.  A `StringDecoder`, this decodes bytes into UTF-8 `String`s or any other encoding of your choice
3.  A `JsonDecoder`, this decodes `String`s using [Gson](https://github.com/google/gson) into objects of type `Person` or any other type of your choice
4.  An anonymous class that simply prints the name of our decoded `Person` to standard output

`JsonDecoder` is not a part of Netty, its implementation is as follows:

{% gist JamieAP/3c3dd2ece49bef05b137 %}
That’s everything we need for our example Netty server to do stuff.

### <a id="seeing_it_in_action_162"></a>Seeing it in action

First we start up the server. How you do this will depend on the way your project is structured:

{% gist JamieAP/d10ae69416dcca57db3d %}

We can then use `telnet` to open a socket to our server:


{% gist JamieAP/5beb7d505373122a8eef %}

And then in standard out we’ll see:

{% gist JamieAP/db3e23b8a4622028bc59 %}
### <a id="wrapping_up_185"></a>Wrapping up

There is a huge amount of detail I’ve glossed over for the sake of making this a very easy introduction and to get you off the ground quickly. The [Netty user guide](http://netty.io/wiki/user-guide-for-4.x.html) goes into more depth and is a good place to start when learning more. If you want to read about NIO in general the [Oracle docs](https://docs.oracle.com/javase//8/docs/technotes/guides/io/index.html) are also helpful.
